package gnukhata.views;

import java.awt.event.KeyAdapter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.TableView.TableRow;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.netTrialBalance;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ControlEditor;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class viewTrialBalReport extends Composite {
	int counter = 0;
	static Display display;
	ODPackage sheetStream;
	TableViewer tblnettrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn dr;
	TableColumn cr;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lbldr;
	Label lblcr;
	Button btnViewTbForAccount;
	Button btnPrint;
    String strdate;
	NumberFormat nf;
	Vector<Object> printNetTrialBalance = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = "";
	int shellwidth = 0;
			
public 	viewTrialBalReport(Composite parent,String endDate, int style, ArrayList<gnukhata.controllers.reportmodels.netTrialBalance  > netData)
	{
		super(parent,style);
		
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		//lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(12);
		lblLine.setLayoutData(layout);
	
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 12, SWT.ITALIC| SWT.BOLD ) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		endDateParam = endDate;
		lblOrgDetails.setText("Net Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(13);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(16);
		lblOrgDetails.setLayoutData(layout);
		
		
		tblnettrialbal = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblnettrialbal.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblnettrialbal.getTable().setLinesVisible (true);
		tblnettrialbal.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(92);
		tblnettrialbal.getTable().setLayoutData(layout);
		
		btnViewTbForAccount =new Button(this,SWT.PUSH);
		btnViewTbForAccount.setText("&Back To Trial Balance");
		btnViewTbForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblnettrialbal.getTable(),15);
		layout.left=new FormAttachment(35);
		btnViewTbForAccount.setLayoutData(layout);
		
		btnPrint =new Button(this,SWT.PUSH);
		btnPrint.setText(" &Print ");
		btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		layout = new FormData();
		layout.top=new FormAttachment(tblnettrialbal.getTable(),15);
		layout.left=new FormAttachment(60);
		btnPrint.setLayoutData(layout);

		
		double dr = 0.00;
		//this.makeaccessible(tblnettrialbal);
		this.getAccessible();
		//this.setEvents();
		//this.pack();
		this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
		shellwidth = this.getClientArea().width;
		try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/NetTrialBal.ots"),"NetTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.setReport(netData);
		setEvents(netData);
		
	}
	
	private void setReport(ArrayList<gnukhata.controllers.reportmodels.netTrialBalance > netData  )
	{

			
		TableViewerColumn colSrNo = new TableViewerColumn(tblnettrialbal, SWT.None);
		colSrNo.getColumn().setText("Sr.No.");
		colSrNo.getColumn().setAlignment(SWT.LEFT);
		colSrNo.getColumn().setWidth(4 * shellwidth /100);
		colSrNo.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				
				

			
				gnukhata.controllers.reportmodels.netTrialBalance net = (gnukhata.controllers.reportmodels.netTrialBalance) element;

				
				return net.getSrNo();
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colaccname = new TableViewerColumn(tblnettrialbal, SWT.None);
		colaccname.getColumn().setText("                                     Account Name");
		colaccname.getColumn().setAlignment(SWT.LEFT);
		colaccname.getColumn().setWidth(36 * shellwidth /100);
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.netTrialBalance accname = (gnukhata.controllers.reportmodels.netTrialBalance) element;
				return accname.getAccountName();
				//return super.getText(element);
			}
		}
		);
		
		
		TableViewerColumn colgrpname = new TableViewerColumn(tblnettrialbal, SWT.None);
		colgrpname.getColumn().setText("                 Group Name");
		colgrpname.getColumn().setAlignment(SWT.LEFT);
		colgrpname.getColumn().setWidth(19 * shellwidth /100);
		colgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.netTrialBalance grpname = (gnukhata.controllers.reportmodels.netTrialBalance) element;
				return grpname.getGroupName();
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn coldr = new TableViewerColumn(tblnettrialbal, SWT.None);
		coldr.getColumn().setText("Debit                      ");
		coldr.getColumn().setAlignment(SWT.RIGHT);
		coldr.getColumn().setWidth(18 * shellwidth /100);
		coldr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.netTrialBalance dr = (gnukhata.controllers.reportmodels.netTrialBalance) element;
				try {
					Double drBal = Double.parseDouble(dr.getDrBal());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(drBal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}

				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colcr = new TableViewerColumn(tblnettrialbal, SWT.None);
		colcr.getColumn().setText("Credit                       ");
		colcr.getColumn().setAlignment(SWT.RIGHT);
		colcr.getColumn().setWidth(16 * shellwidth /100);
		colcr.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.netTrialBalance cr = (gnukhata.controllers.reportmodels.netTrialBalance) element;
				try {
					Double crBal = Double.parseDouble(cr.getCrBal());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(crBal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
					return "";
				}

				//return super.getText(element);
			}
		}
		);
		tblnettrialbal.setContentProvider(new ArrayContentProvider());
		tblnettrialbal.setInput(netData);
		tblnettrialbal.getTable().pack();
		tblnettrialbal.getTable().setFocus();

	}
	private void setEvents(final ArrayList<gnukhata.controllers.reportmodels.netTrialBalance > netData  )
	{
		//now the tableviewr needs to be set the key event and mouse event.
		tblnettrialbal.getControl().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				IStructuredSelection selection = (IStructuredSelection) tblnettrialbal.getSelection();
				netTrialBalance ntb = (netTrialBalance) selection.getFirstElement();
				if(ntb.getGroupName().equals("Total"))
				{
					return;
				}
				String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
				Composite grandParent = (Composite) tblnettrialbal.getTable().getParent().getParent();
				String accName = ntb.getAccountName();
				reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Net Trial Balance","" );
				tblnettrialbal.getTable().getParent().dispose();

				
				//super.mouseDoubleClick(arg0);
			}
		});
		tblnettrialbal.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
				{
					//drilldown here, make a call to showLedger.
					IStructuredSelection selection = (IStructuredSelection) tblnettrialbal.getSelection();
					netTrialBalance ntb = (netTrialBalance) selection.getFirstElement();
					try {
						if(ntb.getGroupName().equals("Total"))
						{
							return;
						}
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent = (Composite) tblnettrialbal.getTable().getParent().getParent();
						String accName = ntb.getAccountName();
						reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Net Trial Balance","" );
						tblnettrialbal.getTable().getParent().dispose();
					} catch (NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					



					
				}
				//super.keyPressed(arg0);
			}
		});
		btnViewTbForAccount.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnPrint.setFocus();
				}
				
			}
		});
		
		
		btnPrint.addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnViewTbForAccount.setFocus();
				}
				
			}
		});
		
		btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				
				Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
				btnViewTbForAccount.getParent().dispose();
					
					viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
					vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
				}
		
		});
				
				btnPrint.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					//super.widgetSelected(arg0);
					String[] strPrintCol = new String[]{"","","","",""};
					Object[][] finaldata=new Object[netData.size() +1][strPrintCol.length];
					Object[] firstRow = new Object[]{"Sr.No.", "Account Name","Group Name","Debit","Credit" };
					finaldata[0] = firstRow;
					
					/*for(int counter=0; counter < netData.size(); counter++)
					{
						Object[] printrow= new Object[5];
						printrow[0] = netData.get(counter).getSrNo();
						printrow[1] = netData.get(counter).getAccountName();
						printrow[2] = netData.get(counter).getGroupName();
						printrow[3] = netData.get(counter).getDrBal();
						printrow[4] = netData.get(counter).getCrBal();
				//		orgdata[counter]=printrow;
						MessageBox mb = new MessageBox(new Shell(), SWT.OK);
						mb.setMessage(printrow[0].toString() );
						mb.open();
						finaldata[counter +1]=printrow;
					}	
					*/
					//printLedgerData.copyInto(finalData);
					
					TableModel model = new DefaultTableModel(finaldata,strPrintCol);
					try 
					{
						final File NetTrialBalReport = new File("/tmp/gnukhata/Report_Output/NetTrialBalance");
						final Sheet NetTrialBalReportSheet = sheetStream.getSpreadSheet().getFirstSheet();
						NetTrialBalReportSheet.ensureRowCount(100000);
						NetTrialBalReportSheet.getCellAt(0, 0).setValue(globals.session[1].toString());
						NetTrialBalReportSheet.getCellAt(0, 1).setValue("Net Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
						for(int rowcounter = 0; rowcounter < netData.size(); rowcounter ++ )
						{
							
							NetTrialBalReportSheet.getCellAt(0,rowcounter +3).setValue(netData.get(rowcounter).getSrNo() );
							NetTrialBalReportSheet.getCellAt(1,rowcounter +3).setValue(netData.get(rowcounter).getAccountName() );
							NetTrialBalReportSheet.getCellAt(2,rowcounter +3).setValue(netData.get(rowcounter).getGroupName());
							NetTrialBalReportSheet.getCellAt(3,rowcounter +3).setValue(netData.get(rowcounter).getDrBal());
							NetTrialBalReportSheet.getCellAt(4,rowcounter +3).setValue(netData.get(rowcounter).getCrBal());
						}
						OOUtils.open(NetTrialBalReportSheet.getSpreadSheet().saveAs(NetTrialBalReport));
						//OOUtils.open(AccountReport);
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			});				
		
		}


	
	
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		/*int vouchercode = 0;
		String voucherType = null;
		viewTrialBalReport vtbr=new viewTrialBalReport(s, SWT.NONE);
		vtbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/
}
