package gnukhata.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import gnukhata.globals;
import gnukhata.controllers.reportController;
import gnukhata.controllers.reportmodels.extendedTrialBalance;
import gnukhata.controllers.reportmodels.grossTrialBalance;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jopendocument.dom.ODPackage;
import org.jopendocument.dom.OOUtils;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import com.sun.istack.internal.FinalArrayList;

public class viewextendedtrialbalreport extends Composite{
	static Display display;
	ODPackage sheetStream;
	int counter=0;
	TableViewer tblextendedtrialbal;
	TableItem headerRow;
	TableColumn srno;
	TableColumn accname;
	TableColumn grpname;
	TableColumn openingbal;
	TableColumn totaldrtransac;
	TableColumn totalcrtransac;
	TableColumn drbal;
	TableColumn crbal;
	Label lblsrno;
	Label lblaccname;
	Label lblgrpname;
	Label lblopeningbal;
	Label lbltotaldrtransac;
	Label lbltotalcrtransac;
	Label lbldrbal;
	Label lblcrbal;
	Button btnViewTbForAccount;
	Button btnPrint;

	NumberFormat nf;
	Vector<Object> printExtendedTrial = new Vector<Object>();
	ArrayList<Button> accounts = new ArrayList<Button>();
	String endDateParam = "";
	int shellwidth = 0;
	int finshellwidth;
	public viewextendedtrialbalreport(Composite parent, String endDate, int style, ArrayList<extendedTrialBalance>extendedData )
	{
		super(parent,style);
		endDateParam = endDate;
		FormLayout formlayout = new FormLayout();
		FormData layout=new FormData();
		this.setLayout(formlayout);
		String strdate;
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		Label lblLogo = new Label(this, SWT.None);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(63);
		layout.right = new FormAttachment(87);
		layout.bottom = new FormAttachment(9);
		//layout.right = new FormAttachment(95);
		//layout.bottom = new FormAttachment(18);
		//lblLogo.setSize(getClientArea().width, getClientArea().height);
		lblLogo.setLocation(getClientArea().width, getClientArea().height);
		lblLogo.setLayoutData(layout);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD ) );
		lblOrgDetails.setText(globals.session[1]+"\n"+"For Financial Year "+"From "+globals.session[2]+" To "+globals.session[3] );
		layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		//layout.right = new FormAttachment(53);
		//layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);

		/*Label lblLink = new Label(this,SWT.None);
		lblLink.setText("www.gnukhata.org");
		lblLink.setFont(new Font(display, "Times New Roman", 11, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(lblLogo,0);
		layout.left = new FormAttachment(65);
		//layout.right = new FormAttachment(33);
		//layout.bottom = new FormAttachment(19);
		lblLink.setLayoutData(layout);*/
		 
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman",18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment( lblLogo , 2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(12);
		lblLine.setLayoutData(layout);
		
		lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 12, SWT.ITALIC| SWT.BOLD ) );
		strdate=endDate.substring(8)+"-"+endDate.substring(5,7)+"-"+endDate.substring(0, 4);
		lblOrgDetails.setText("Extended Trial Balance For The Period From "+globals.session[2]+" To "+strdate);
		layout = new FormData();
		layout.top = new FormAttachment(13);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(16);
		lblOrgDetails.setLayoutData(layout);
		
		//tblextendedtrialbal = new Table (this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);
		tblextendedtrialbal = new TableViewer(this, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION|SWT.LINE_SOLID);
		tblextendedtrialbal.getTable().setFont(new Font(display,"UBUNTU",10,SWT.BOLD));
		tblextendedtrialbal.getTable().setLinesVisible (true);
		tblextendedtrialbal.getTable().setHeaderVisible (true);
		layout = new FormData();
		layout.top = new FormAttachment(lblOrgDetails,10);
		layout.left = new FormAttachment(1);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(92);
		tblextendedtrialbal.getTable().setLayoutData(layout);
		
		
		  btnViewTbForAccount =new Button(this,SWT.PUSH);
		  btnViewTbForAccount.setText("&Back To Trial Balance");
		  btnViewTbForAccount.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
		  layout = new FormData();
		  layout.top=new FormAttachment(tblextendedtrialbal.getTable(),15);
		  layout.left=new FormAttachment(35);
		  btnViewTbForAccount.setLayoutData(layout);

			
		    btnPrint =new Button(this,SWT.PUSH);
			btnPrint.setText(" &Print ");
			btnPrint.setFont(new Font(display,"Times New Roman",10,SWT.BOLD));
			layout = new FormData();
			layout.top=new FormAttachment(tblextendedtrialbal.getTable(),15);
			layout.left=new FormAttachment(60);
			btnPrint.setLayoutData(layout);

		
		
		
	
	//this.makeaccessible(tblextendedtrialbal);
	this.getAccessible();
	//this.setEvents();
	//this.pack();
	this.setBounds(this.getDisplay().getPrimaryMonitor().getBounds());
	shellwidth = this.getClientArea().width;
	finshellwidth = shellwidth-(shellwidth*2/100);
	try {
			sheetStream = ODPackage.createFromStream(this.getClass().getResourceAsStream("/templates/ExtendedTrialBal.ots"),"ExtendedTrialBal");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	this.setReport(extendedData);
	this.setEvents(extendedData);
	
}
	
	private void setReport(ArrayList<extendedTrialBalance> extendedData)
	{
		
		TableViewerColumn colSrNo = new TableViewerColumn(tblextendedtrialbal, SWT.None);
		colSrNo.getColumn().setText("Sr.No.");
		colSrNo.getColumn().setAlignment(SWT.LEFT);
		colSrNo.getColumn().setWidth(4 * finshellwidth /100);
		colSrNo.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
			gnukhata.controllers.reportmodels.extendedTrialBalance srNo = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
			return srNo.getSrNo();
			}
		});
		
		TableViewerColumn colaccname = new TableViewerColumn(tblextendedtrialbal,SWT.None);
		colaccname.getColumn().setText("          Account Name");
		colaccname.getColumn().setWidth(21 * finshellwidth /100);
		colaccname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance accname = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				return accname.getAccountName();
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colgrpname = new TableViewerColumn(tblextendedtrialbal, SWT.None);
		colgrpname.getColumn().setText("            Group Name");
		colgrpname.getColumn().setAlignment(SWT.LEFT);
		colgrpname.getColumn().setWidth(18 * finshellwidth /100);
		colgrpname.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance grpname = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				return grpname.getGroupName();
				//return super.getText(element);
			}
		}
		);
		TableViewerColumn colopeningbal = new TableViewerColumn(tblextendedtrialbal, SWT.None);
		colopeningbal.getColumn().setText("Opening Balance       ");
		colopeningbal.getColumn().setAlignment(SWT.RIGHT);
		colopeningbal.getColumn().setWidth(10 * finshellwidth /100);
		colopeningbal.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance openingbal = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				try {
					Double openingbalance = Double.parseDouble(openingbal.getOpeningBalance());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(openingbalance);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					if(openingbal.getOpeningBalance().equals(""))
					{
						return "";
					}
					else if (openingbal.getOpeningBalance().equals("Total"))
					{
						return "Total";
					}
					else
					{
						return "";
					}
				}
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn coltotaldrtransactions = new TableViewerColumn(tblextendedtrialbal, SWT.None);
		coltotaldrtransactions.getColumn().setText("Tot Dr Transactions");
		coltotaldrtransactions.getColumn().setAlignment(SWT.RIGHT);
		coltotaldrtransactions.getColumn().setWidth(11 * finshellwidth/100);
		coltotaldrtransactions.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance totaldrtransactions = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				try {
					Double totaldrtran = Double.parseDouble(totaldrtransactions.getTotalDrTransactions());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(totaldrtran);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn coltotalcrtransactions = new TableViewerColumn(tblextendedtrialbal, SWT.None);
		coltotalcrtransactions.getColumn().setText("Tot Cr Transactions");
		coltotalcrtransactions.getColumn().setAlignment(SWT.RIGHT);
		coltotalcrtransactions.getColumn().setWidth(11 * finshellwidth /100);
		coltotalcrtransactions.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance totalcrtransactions = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				try {
					Double totalcrtran = Double.parseDouble(totalcrtransactions.getTotalCrTransactions());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(totalcrtran);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn coldrbalance= new TableViewerColumn(tblextendedtrialbal, SWT.None);
		coldrbalance.getColumn().setText("Debit Balance            ");
		coldrbalance.getColumn().setAlignment(SWT.RIGHT);
		coldrbalance.getColumn().setWidth(10 * finshellwidth /100);
		coldrbalance.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance drbalance = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				try {
					Double drbal = Double.parseDouble(drbalance.getDrBalance());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(drbal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					
					return "";
					
				}
				//return super.getText(element);
			}
		}
		);
		
		TableViewerColumn colcrbalance= new TableViewerColumn(tblextendedtrialbal, SWT.None);
		colcrbalance.getColumn().setText("Credit Balance            ");
		colcrbalance.getColumn().setAlignment(SWT.RIGHT);
		colcrbalance.getColumn().setWidth(7 * finshellwidth /100);
		colcrbalance.setLabelProvider(new ColumnLabelProvider()
		{
			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				gnukhata.controllers.reportmodels.extendedTrialBalance crbalance = (gnukhata.controllers.reportmodels.extendedTrialBalance) element;
				try {
					Double crbal = Double.parseDouble(crbalance.getCrBalance());
					nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);
					return nf.format(crbal);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					return "";
					
				}
			}
		}
		);
		
		tblextendedtrialbal.setContentProvider(new ArrayContentProvider());
		tblextendedtrialbal.setInput(extendedData);
		tblextendedtrialbal.getTable().pack();
		tblextendedtrialbal.getTable().setFocus();
		
		}
private void setEvents(final ArrayList<extendedTrialBalance> extendeddata)
{	
	tblextendedtrialbal.getControl().addMouseListener(new MouseAdapter() {
		@Override
		public void mouseDoubleClick(MouseEvent arg0) {
			// TODO Auto-generated method stub
			IStructuredSelection selection = (IStructuredSelection) tblextendedtrialbal.getSelection();
			extendedTrialBalance etb = (extendedTrialBalance) selection.getFirstElement();
			if(etb.getGroupName().equals("Total") || etb.getAccountName().equals("Difference in Trial Balance"))
			{
				return;
			}
			String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
			Composite grandParent = (Composite) tblextendedtrialbal.getTable().getParent().getParent();
			String accName = etb.getAccountName();
			reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Gross Trial Balance","" );
			tblextendedtrialbal.getTable().getParent().dispose();

			
			//super.mouseDoubleClick(arg0);
		}
	});

	
	
	tblextendedtrialbal.getControl().addKeyListener(new org.eclipse.swt.events.KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.keyCode == SWT.CR || arg0.keyCode== SWT.KEYPAD_CR)
			{
				//drilldown here, make a call to showLedger.
				IStructuredSelection selection = (IStructuredSelection) tblextendedtrialbal.getSelection();
				extendedTrialBalance etb = (extendedTrialBalance) selection.getFirstElement();
				try {
					if(etb.getGroupName().equals("Total") || etb.getAccountName().equals("Difference in Trial Balance"))
						{
							return;
						}
						String fromdate=globals.session[2].toString().substring(6)+"-"+globals.session[2].toString().substring(3,5)+"-"+globals.session[2].toString().substring(0,2);
						Composite grandParent = (Composite) tblextendedtrialbal.getTable().getParent().getParent();
						String accName = etb.getAccountName();
						reportController.showLedger(grandParent, accName,fromdate,endDateParam, "No Project", true, true, false,"Extended Trial Balance","" );
						tblextendedtrialbal.getTable().getParent().dispose();
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		}

		});
	

	btnViewTbForAccount.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			
			
			Composite grandParent = (Composite) btnViewTbForAccount.getParent().getParent();
			btnViewTbForAccount.getParent().dispose();
				
				viewTrialBalance vl=new viewTrialBalance(grandParent,SWT.NONE);
				vl.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
			}
	
	});
	
	btnPrint.addKeyListener(new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent arg0) {
			// TODO Auto-generated method stub
			//super.keyPressed(arg0);
			if(arg0.keyCode==SWT.ARROW_LEFT)
			{
				btnViewTbForAccount.setFocus();
			}
		}
	});
		
			/*accounts.get(accountcounter).addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0);
				if(arg0.keyCode==SWT.ARROW_DOWN && counter < accounts.size()-1 )
				{
					counter++;
					if(counter >= 0 && counter < accounts.size())
					{
						
					accounts.get(counter).setFocus();
					}
				}
				if(arg0.keyCode==SWT.ARROW_UP && counter > 0)
				{
					counter--;
					if(counter >= 0&& counter < accounts.size())
					{
						
						accounts.get(counter).setFocus();
					}
				}
			
				
			}

		});
	}
	
*/	btnPrint.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			//super.widgetSelected(arg0);
			/*String[] strPrintCol = new String[]{"","","","",""};
			Object[][] finaldata=new Object[extendeddata.size() +1][strPrintCol.length];
			Object[] firstRow = new Object[]{"Sr.No","Account Name","Group Name","Total Debit","Total Credit" };
			finaldata[0] = firstRow;*/
			/*for(int counter = 0; counter < printExtendedTrial.size(); counter ++ )
			{
				Object[] printRow = (Object[]) printExtendedTrial.get(counter);												
				finalData[counter] = printRow;
			}*/
				
			//printLedgerData.copyInto(finalData);
			
				//TableModel model = new DefaultTableModel(finalData,strPrintCol);
				try {
					final File extended = new File("/tmp/gnukhata/Report_Output/ExtendedTrialBalance" );
					/*SpreadSheet.createEmpty(model).saveAs(extended);*/
					final Sheet extendedSheet = sheetStream.getSpreadSheet().getFirstSheet();
					extendedSheet.ensureRowCount(100000);
					/*extendedSheet.getColumn(0).setWidth(new Integer(30));
					extendedSheet.getColumn(1).setWidth(new Integer(50));
					extendedSheet.getColumn(2).setWidth(new Integer(50));
					extendedSheet.getColumn(3).setWidth(new Integer(35));
					extendedSheet.getColumn(4).setWidth(new Integer(45));
					extendedSheet.getColumn(5).setWidth(new Integer(45));
					extendedSheet.getColumn(6).setWidth(new Integer(40));
					extendedSheet.getColumn(7).setWidth(new Integer(40));
					*/
					extendedSheet.getCellAt(0,0).setValue(globals.session[1].toString());
					extendedSheet.getCellAt(0,1).setValue("Extended Trial Balance For The Period From "+globals.session[2]+" To "+ globals.session[3]);
					for(int rowcounter=0; rowcounter<extendeddata.size(); rowcounter++)
					{
						
						//OOUtils.open(extendedSheet.getSpreadSheet().saveAs(extended));
						extendedSheet.getCellAt(0, rowcounter+3).setValue(extendeddata.get(rowcounter).getSrNo());
						extendedSheet.getCellAt(1, rowcounter+3).setValue(extendeddata.get(rowcounter).getAccountName());
						extendedSheet.getCellAt(2, rowcounter+3).setValue(extendeddata.get(rowcounter).getGroupName());
						extendedSheet.getCellAt(3, rowcounter+3).setValue(extendeddata.get(rowcounter).getOpeningBalance());
						extendedSheet.getCellAt(4, rowcounter+3).setValue(extendeddata.get(rowcounter).getTotalDrTransactions());
						extendedSheet.getCellAt(5, rowcounter+3).setValue(extendeddata.get(rowcounter).getTotalCrTransactions());
						extendedSheet.getCellAt(6, rowcounter+3).setValue(extendeddata.get(rowcounter).getDrBalance());
						extendedSheet.getCellAt(7, rowcounter+3).setValue(extendeddata.get(rowcounter).getCrBalance());
						//				OOUtils.open(extended);
					}
					OOUtils.open(extendedSheet.getSpreadSheet().saveAs(extended));
				} 
					catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	});
	
}
	
	public void makeaccessible(Control c)
	{
	/*
	 * getAccessible() method is the method of class Controlwhich is the
	 * parent class of all the UI components of SWT including Shell.so when
	 * the shell is made accessible all the controls which are contained by
	 * that shell are made accessible automatically.
	 */
		c.getAccessible();
	}



	protected void checkSubclass()
	{
	//this is blank method so will disable the check that prevents subclassing of shells.
	}
	/*public static void main(String[] args)
	{
		Display d = new Display();
		Shell s= new Shell(d);
		int vouchercode = 0;
		String voucherType = null;
		Object[] tbData2 = null;
		viewextendedtrialbalreport vetbr=new viewextendedtrialbalreport(s, SWT.NONE, tbData2);
		vetbr.setSize(s.getClientArea().width, s.getClientArea().height );
		
		//s.setSize(400, 400);
		s.pack();
		s.open();
		while (!s.isDisposed() ) {
			if (!d.readAndDispatch())
			{
				 d.sleep();
				 if(! s.getMaximized())
				 {
					 s.setMaximized(true);
				 }
			}
		}
		
	}*/
}
