package gnukhata.views;

/*
 * @authors
 * Amit Chougule <acamit333@gmail.com>,
 * Girish Joshi <girish946@gmail.com>, 
 */


import gnukhata.globals;
import gnukhata.controllers.StartupController;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;

/*
 * this class is the loginform for the gnukhata.
 */
public class LoginForm extends Shell
{
	static Display display;
	String strOrgName;
	String strFromYear;
	String strToYear;
	String strype;
	Label lblRegiNo;
	Label lblUserName;
	Text txtUserName;
	Label lblPassword;
	Text txtPassword;
	Button btnLogin;
	Button btnBack;
	public LoginForm() {
		super(Display.getDefault());
		strOrgName = globals.session[1].toString();
		strFromYear =  globals.session[2].toString();
		strToYear =  globals.session[3].toString();
		FormLayout formlayout = new FormLayout();
		this.setLayout(formlayout);
		this.setText("Login Form");
		
		Label lblWelcome = new Label(this,SWT.None);
		lblWelcome.setText("Welcome");
		lblWelcome.setFont(new Font(display, "Times New Roman", 14, SWT.BOLD));
		FormData layout = new FormData();
		layout.top = new FormAttachment(2);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(20);
		layout.bottom = new FormAttachment(5);
		lblWelcome.setLayoutData(layout);
		
		Label lblHeadline = new Label(this,SWT.None);
		lblHeadline.setFont(new Font(display, "Times New Roman", 13, SWT.BOLD));
		lblHeadline.setText("GNUKhata: A Free and Open Source Accounting Software");
		layout = new FormData();
		layout.top = new FormAttachment(lblWelcome,1);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(51);
		layout.bottom = new FormAttachment(8);
		lblHeadline.setLayoutData(layout);
		
		Label lblLogo = new Label(this, SWT.None);
		//Image img = new Image(display,"finallogo1.png");
		lblLogo.setImage(globals.logo);
		layout = new FormData();
		layout.top = new FormAttachment(1);
		layout.left = new FormAttachment(70);
		layout.right = new FormAttachment(100);
		layout.bottom = new FormAttachment(12);
		lblLogo.setLayoutData(layout);
		
		
		Label lblOrgDetails = new Label(this,SWT.NONE);
		lblOrgDetails.setFont( new Font(display,"Times New Roman", 11, SWT.BOLD) );
		lblOrgDetails.setText(strOrgName+"\n"+"For Financial Year "+"From "+strFromYear+" To "+strToYear );
		layout = new FormData();
		layout.top = new FormAttachment(10);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(69);
		layout.bottom = new FormAttachment(18);
		lblOrgDetails.setLayoutData(layout);
		
		Label lblLine = new Label(this,SWT.NONE);
		lblLine.setText("-------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		lblLine.setFont(new Font(display, "Times New Roman", 18, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(20);
		layout.left = new FormAttachment(2);
		layout.right = new FormAttachment(99);
		layout.bottom = new FormAttachment(26);
		lblLine.setLayoutData(layout);
		
		lblUserName = new Label(this, SWT.NONE);
		lblUserName.setText("&User Name :");
		lblUserName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(46);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(49);
		lblUserName.setLayoutData(layout);
		
		txtUserName = new Text(this, SWT.BORDER);
		txtUserName.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtUserName.setText("admin");
		layout = new FormData();
		layout.top = new FormAttachment(45);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(49);
		txtUserName.setLayoutData(layout);
		
		lblPassword = new Label(this, SWT.NONE);
		lblPassword.setText("&Password :");
		lblPassword.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		layout = new FormData();
		layout.top = new FormAttachment(51);
		layout.left = new FormAttachment(35);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(54);
		lblPassword.setLayoutData(layout);
		
		txtPassword = new Text(this, SWT.BORDER);
		txtPassword.setFont(new Font(display,"Times New Romen",10,SWT.NONE));
		txtPassword.setEchoChar('*');
		txtPassword.setMessage("admin");
		layout = new FormData();
		layout.top = new FormAttachment(50);
		layout.left = new FormAttachment(45);
		layout.right = new FormAttachment(55);
		layout.bottom = new FormAttachment(54);
		txtPassword.setLayoutData(layout);
		
		btnLogin = new Button(this,SWT.PUSH);
		btnLogin.setText("&Login");
		btnLogin.setFont(new Font(display, "Times New Roman", 10, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(40);
		layout.right = new FormAttachment(45);
		layout.bottom = new FormAttachment(65);
		btnLogin.setLayoutData(layout);
		
		btnBack = new Button(this,SWT.PUSH);
		btnBack.setText("&Back");
		btnBack.setFont(new Font(display, "Times New Roman", 10, SWT.ITALIC));
		layout = new FormData();
		layout.top = new FormAttachment(60);
		layout.left = new FormAttachment(47);
		layout.right = new FormAttachment(52);
		layout.bottom = new FormAttachment(65);
		btnBack.setLayoutData(layout);
		this.setImage(globals.icon);
		this.getAccessible();
		this.setEvents();
		this.pack();
		this.open();
		this.showView();
		}
	private void setEvents()
	{
		txtPassword.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				//super.keyPressed(e);
				if(e.keyCode == SWT.CR | e.keyCode==SWT.KEYPAD_CR)
				{	
					btnLogin.notifyListeners(SWT.Selection, new Event());
					
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtUserName.selectAll();
					txtUserName.setFocus();
				}
			}
		});
		btnLogin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				if (txtUserName.getText().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please enter a User name");
					alert.open();
					txtUserName.setFocus();
					return;
				}
				if(txtPassword.getText().equals(""))
				{
					MessageBox alert = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					alert.setText("Error!");
					alert.setMessage("Please enter a password");
					alert.open();
					txtPassword.setFocus();
					return;
				}
				
				if (StartupController.login(txtUserName.getText(),txtPassword.getText()))
				{
					/*MessageBox msg = new MessageBox(new Shell(),SWT.OK);
					msg.setMessage("Login Successful");
					msg.open();*/
					dispose();
					StartupController.showMainShell(display, 2);
				}
				else
				{
					MessageBox msg = new MessageBox(new Shell(),SWT.OK| SWT.ERROR);
					msg.setMessage("Please enter valid Username and Password");
					msg.open();
					txtUserName.selectAll();
					txtUserName.setFocus();
					
					
				}
				}
				
		});
		
		
		txtUserName.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.keyCode==SWT.CR||e.keyCode==SWT.KEYPAD_CR)
				{
					if(txtUserName.isEnabled())
					{
						txtPassword.setFocus();
					}
				}
				if(e.keyCode==SWT.ARROW_UP)
				{
					txtUserName.setFocus();					
				}
			}
		});
		
		btnLogin.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0)
				if(arg0.keyCode==SWT.ARROW_RIGHT)
				{
					btnBack.setFocus();
				}
			}
		});
		
		btnBack.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				//super.keyPressed(arg0)
				if(arg0.keyCode==SWT.ARROW_LEFT)
				{
					btnLogin.setFocus();
				}
			}
		});
		
		btnBack.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//super.widgetSelected(arg0);
				
				btnBack.getParent().dispose();
				startupForm sf = new startupForm();
			}
		});
					
	}
	
	public void makeaccessible(Control c)
	{
		/*
		 * getAccessible() method is the method of class Controlwhich is the
		 * parent class of all the UI components of SWT including Shell.so when
		 * the shell is made accessible all the controls which are contained by
		 * that shell are made accessible automatically.
		 */
		c.getAccessible();
	}


	
	protected void checkSubclass()
	{
		//this is blank method so will disable the check that prevents subclassing of shells.
	}
	private void showView()
	{
		while(! this.isDisposed())
		{
			if(! this.getDisplay().readAndDispatch())
			{
				this.getDisplay().sleep();
				if ( ! this.getMaximized())
				{
					this.setMaximized(true);
				}
			}
			
		}
		this.dispose();


	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		LoginForm lf = new LoginForm();
		
		

	}

}
