package gnukhata.controllers;

import java.io.PrintStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.xmlrpc.XmlRpcException;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;

import sun.org.mozilla.javascript.ast.CatchClause;
import gnukhata.globals;
import gnukhata.controllers.reportmodels.accountReport;
import gnukhata.controllers.reportmodels.extendedTrialBalance;
import gnukhata.controllers.reportmodels.grossTrialBalance;
import gnukhata.controllers.reportmodels.netTrialBalance;
import gnukhata.controllers.reportmodels.transaction;
import gnukhata.views.AccountReport;
import gnukhata.views.LedgerRecon;
import gnukhata.views.ViewBalanceSheetReport;
import gnukhata.views.ViewCashFlowReport;
import gnukhata.views.ViewDualLedgr;
import gnukhata.views.ViewLedgerReport;
import gnukhata.views.ViewSourcesOfFundBalanceSheet;
import gnukhata.views.ViewUnclearedAccounts;
import gnukhata.views.getUnclearedTransactions;
import gnukhata.views.updateBankRecon;
import gnukhata.views.viewBalanceSheet;
import gnukhata.views.viewCashflow;
import gnukhata.views.viewProfitAndLossReport;
import gnukhata.views.viewProjectStatementReport;
import gnukhata.views.viewReconciliation;
import gnukhata.views.viewTrialBalReport;
import gnukhata.views.viewextendedtrialbalreport;
import gnukhata.views.viewgrosstrialbalreport;

public class reportController {
	
	private static final String ProjectName = null;
	
	public static void showBalanceSheet (Composite grandParent, String endDate, String tbType)
	{
		List<Object> serverParams = new ArrayList<Object>();
		List<Object> serverParams2 = new ArrayList<Object>();
		serverParams2.add(new Object[]{globals.session[2],globals.session[2],endDate});
		serverParams2.add(globals.session[0]);
		serverParams.add(new Object[]{globals.session[2], globals.session[2] ,endDate});
		serverParams.add(globals.session[0]);
		try {
			Object[] profitloss = (Object[]) globals.client.execute("reports.getProfitLoss", serverParams2);
			Object[] result = (Object[]) globals.client.execute("reports.getBalancesheet" , serverParams);
			if(tbType.equals("Conventional Balance Sheet") | tbType.equals("Conventional Statement of Affairs"))
			{
			
				ViewBalanceSheetReport bsr = new ViewBalanceSheetReport(grandParent, SWT.NONE, endDate, result ,profitloss);
				bsr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Sources & Application of fund") | tbType.equals("Sources & Application of Funds"))
			{
				ViewSourcesOfFundBalanceSheet fbsr = new ViewSourcesOfFundBalanceSheet(grandParent, SWT.NONE, endDate, result, profitloss);
				fbsr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
		

		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		
		
	}

	public static void showTrialBalance (Composite grandParent, String endDate, String tbType)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{globals.session[2], globals.session[2], endDate});
		serverParams.add(globals.session[0]);
		try {
			if(tbType.equals("Net Trial Balance"))
			{
				
				Object[] tbData = (Object[]) globals.client.execute("reports.getTrialBalance", serverParams);
				ArrayList<netTrialBalance> netData = new ArrayList<netTrialBalance>();
				for(int tbcounter = 0; tbcounter < tbData.length; tbcounter ++ )
				{
					Object[] tbRow = (Object[]) tbData[tbcounter];
					if(tbcounter < tbData.length -1 )
					{
						String srNo = tbRow[0].toString();
						System.out.println(srNo);
						String accountName = tbRow[1].toString();
						System.out.println(accountName);
						String groupName = tbRow[2].toString();
						System.out.println(groupName);
						String drBal = tbRow[3].toString();
						System.out.println(drBal);
						String crBal = tbRow[4].toString();
						System.out.println(crBal);
						netTrialBalance ntb = new netTrialBalance(srNo, accountName, groupName, drBal, crBal);
						netData.add(ntb);
						System.out.println("now from the list of instances");
						System.out.println(netData.get(tbcounter).getSrNo() + ", " + netData.get(tbcounter).getAccountName() );
					}
					else
					{
						String srNo = "";
						String accountName ="";
						String groupName = "Total";
						String drBal =  tbRow[0].toString();
						String crBal = tbRow[1].toString();
						netTrialBalance ntb = new netTrialBalance(srNo, accountName, groupName, drBal, crBal);
						netData.add(ntb);
						
					}
					
				}
				viewTrialBalReport tbr = new viewTrialBalReport(grandParent, endDate, SWT.NONE , netData);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Gross Trial Balance"))
			{
				
				Object[] tbData1 = (Object[]) globals.client.execute("reports.getGrossTrialBalance", serverParams);
				ArrayList<grossTrialBalance> grossdata = new ArrayList<grossTrialBalance>();
				String tdr = "";
				String tcr= "";
				double diffbal=0.00;
				double dr=0.00;
				double cr=0.00;
				for(int tbcounter = 0; tbcounter< tbData1.length; tbcounter ++)
				{
					
					Object[] tbRow = (Object[]) tbData1[tbcounter];
					if(tbcounter < tbData1.length-1)
					{
						String srNo = tbRow[0].toString();
						System.out.println(srNo);
						String accountName = tbRow[1].toString();
						System.out.println(accountName);
						String groupName = tbRow[2].toString();
						System.out.println(groupName);
						String totaldr = tbRow[3].toString();
						System.out.println(totaldr);
						String totalcr = tbRow[4].toString();
						System.out.println(totalcr);
						grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
						grossdata.add(gtb);
						System.out.println("now from the list of instances");
						System.out.println(grossdata.get(tbcounter).getSrNo() + ", " + grossdata.get(tbcounter).getAccountName() );
					}
					else
					{
						String srNo = "";
						String accountName ="";
						String groupName = "Total";
						String totaldr =  tbRow[0].toString();
						String totalcr = tbRow[1].toString();
						grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
						grossdata.add(gtb);
						
					}
					
					
				}
				
				/*TableItem closingRow = new TableItem(tblgrosstrialbal , SWT.NONE);
				//closingRow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
				closingRow.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
				*///now get the Total Dr and Total Cr.
				//both can be got from the last row of tbdata.
				//get the last row (tbdata.len -1)
				//then access row[0] for TotalDr and row[1] for totalCr.
				//if dr is greater than cr then it is a dr balance.
				//substract the cr amount from dr to get the diff.
				//do exactly the other way round for cr > dr.
				
				/*nf = NumberFormat.getInstance();
				nf.setGroupingUsed(false);
				nf.setMaximumFractionDigits(2);
				nf.setMinimumFractionDigits(2);*/
				
				Object[] lastRow = (Object[]) tbData1[tbData1.length-1 ];
				dr= Double.parseDouble(lastRow[0].toString());
				cr= Double.parseDouble(lastRow[1].toString());
				
				if(dr > cr)
				{
					diffbal = dr - cr;
					String srNo = "";
					String accountName ="";
					String groupName = "Difference In Trial Balance";
					String totaldr =  "";
					String totalcr = Double.toString(diffbal);
					grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
					grossdata.add(gtb);

				}
				if(cr > dr)
				{
					diffbal = cr - dr;
					String srNo = "";
					String accountName ="";
					String groupName = "Difference In Trial Balance";
					String totaldr = Double.toString(diffbal);
					String totalcr = "";
					grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
					grossdata.add(gtb);
				}
				
				/*TableItem totaldrcr = new TableItem(tblgrosstrialbal , SWT.NONE|SWT.SEPARATOR|SWT.BOLD);
				//totaldrcr.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
				totaldrcr.setFont(new Font(display, "Times New Roman",10,SWT.BOLD|SWT.CENTER));*/
				if(dr>cr)
				{
					String srNo = "";
					String accountName ="";
					String groupName = "";
					String totaldr = Double.toString(dr);
					String totalcr = Double.toString(dr);
					grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
					grossdata.add(gtb);
				}
				if(cr>dr)
				{
					String srNo = "";
					String accountName ="";
					String groupName = "";
					String totaldr = Double.toString(cr);
					String totalcr = Double.toString(cr);
					grossTrialBalance gtb = new grossTrialBalance(srNo, accountName, groupName, totaldr, totalcr);
					grossdata.add(gtb);
				}

								
				viewgrosstrialbalreport tbr = new viewgrosstrialbalreport(grandParent,endDate, SWT.NONE , grossdata);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			if(tbType.equals("Extended Trial Balance"))
			{
				
				Object[] tbData2 = (Object[]) globals.client.execute("reports.getExtendedTrialBalance", serverParams);
				ArrayList<extendedTrialBalance> extendedData = new ArrayList<extendedTrialBalance>();
				String tdr1 = "";
				String tcr1= "";
				double diffbal1=0.00;
				double dr1=0.00;
				double cr1=0.00;
				
				for(int extendedcounter = 0; extendedcounter < tbData2.length;extendedcounter ++)
				{
										
						Object[] tbRow = (Object[]) tbData2[extendedcounter];
						if(extendedcounter < tbData2.length-1)
						{
							String srNo = tbRow[0].toString();
							System.out.println(srNo);
							String accountName = tbRow[1].toString();
							System.out.println(accountName);
							String groupName = tbRow[2].toString();
							System.out.println(groupName);
							String openingBalance = tbRow[3].toString();
							System.out.println(openingBalance);
							String totalDrTransactions = tbRow[4].toString();
							System.out.println(totalDrTransactions);
							String totalCrTransactions = tbRow[5].toString();
							System.out.println(totalCrTransactions);
							String drBalance = tbRow[6].toString();
							System.out.println(drBalance);
							String crBalance = tbRow[7].toString();
							System.out.println(crBalance);
							extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
							extendedData.add(etb);
							System.out.println("now from the list of instances");
							System.out.println(extendedData.get(extendedcounter).getSrNo() + ", " + extendedData.get(extendedcounter).getAccountName() );
						}
						else
						{
							String srNo = "";
							
							String accountName = "";
							
							String groupName = "";
							
							String openingBalance = "Total";
							
							String totalDrTransactions = tbRow[2].toString();
							
							String totalCrTransactions = tbRow[3].toString();
							
							String drBalance = tbRow[0].toString();
							
							String crBalance = tbRow[1].toString();
							
							extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
							extendedData.add(etb);

							
						}
						
						
					}
					
					/*TableItem closingRow = new TableItem(tblgrosstrialbal , SWT.NONE);
					//closingRow.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
					closingRow.setFont(new Font(display, "Times New Roman",10,SWT.BOLD));
					*///now get the Total Dr and Total Cr.
					//both can be got from the last row of tbdata.
					//get the last row (tbdata.len -1)
					//then access row[0] for TotalDr and row[1] for totalCr.
					//if dr is greater than cr then it is a dr balance.
					//substract the cr amount from dr to get the diff.
					//do exactly the other way round for cr > dr.
					
					/*nf = NumberFormat.getInstance();
					nf.setGroupingUsed(false);
					nf.setMaximumFractionDigits(2);
					nf.setMinimumFractionDigits(2);*/
					
					Object[] lastRow = (Object[]) tbData2[tbData2.length-1 ];
					dr1= Double.parseDouble(lastRow[0].toString());
					cr1= Double.parseDouble(lastRow[1].toString());
					
					if(dr1 > cr1)
					{
						diffbal1 = dr1 - cr1;
						String srNo = "";
						
						String accountName = "Difference in Trial Balance";
						
						String groupName = "";
						
						String openingBalance = "";
						
						String totalDrTransactions = "";
						
						String totalCrTransactions = "";
						
						String drBalance = "";
						
						String crBalance = Double.toString(diffbal1);
						
						extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
						extendedData.add(etb);

					}
					if(cr1 > dr1)
					{
						diffbal1 = cr1 - dr1;
						String srNo = "";
						
						String accountName = "Difference in Trial Balance";
						
						String groupName = "";
						
						String openingBalance = "";
						
						String totalDrTransactions = "";
						
						String totalCrTransactions = "";
						
						String drBalance = Double.toString(diffbal1);
						
						String crBalance = "";
						
						extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
						extendedData.add(etb);

					}
					
					/*TableItem totaldrcr = new TableItem(tblgrosstrialbal , SWT.NONE|SWT.SEPARATOR|SWT.BOLD);
					//totaldrcr.setFont(new Font(display, "Times New Roman", 10, SWT.NORMAL));
					totaldrcr.setFont(new Font(display, "Times New Roman",10,SWT.BOLD|SWT.CENTER));*/
					if(dr1>cr1)
					{
						String srNo = "";
						
						String accountName = "";
						
						String groupName = "";
						
						String openingBalance = "";
						
						String totalDrTransactions = "";
						
						String totalCrTransactions = "";
						
						String drBalance = Double.toString(dr1);
						
						String crBalance = Double.toString(dr1);
						
						extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
						extendedData.add(etb);

					}
					if(cr1>dr1)
					{
						String srNo = "";
						
						String accountName = "";
						
						String groupName = "";
						
						String openingBalance = "";
						
						String totalDrTransactions = "";
						
						String totalCrTransactions = "";
						
						String drBalance = Double.toString(cr1);
						
						String crBalance = Double.toString(cr1);
						
						extendedTrialBalance etb = new extendedTrialBalance(srNo, accountName, groupName, openingBalance, totalDrTransactions, totalCrTransactions, drBalance, crBalance);
						extendedData.add(etb);
					}
	
					
				
				viewextendedtrialbalreport tbr = new viewextendedtrialbalreport(grandParent,endDate, SWT.NONE , extendedData);
				tbr.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			
			
		}
		catch(XmlRpcException e)
		{
			e.printStackTrace();
		}
	} 
		
	
	
	
	public static void showProfitAndLoss(Composite grandParent,String toDate)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{globals.session[2], globals.session[2],toDate});
		
		
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("reports.getProfitLoss" , serverParams);
			viewProfitAndLossReport vplr=new viewProfitAndLossReport(grandParent, SWT.NONE, toDate, result);
			vplr.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
		} 
		catch (XmlRpcException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void showProjectStatement(Composite grandParent,String toDate, String selectproject)
	{
		List<Object> serverParams = new ArrayList<Object>();
		serverParams.add(new Object[]{selectproject,globals.session[2], globals.session[2], toDate});
		
		
		serverParams.add(globals.session[0]);
		try {
			Object[] result = (Object[]) globals.client.execute("reports.getProjectStatement" , serverParams);
			viewProjectStatementReport vprs = new viewProjectStatementReport(grandParent, toDate, SWT.NONE, result, selectproject);
			vprs.setSize(grandParent.getClientArea().width,grandParent.getClientArea().height);
		} 
		catch (XmlRpcException e)
		{
			e.printStackTrace();
		}
	}
	public static void showLedger(Composite grandParent, String accountName,String fromDate,String toDate,String ProjectName, boolean narrationFlag, boolean tbDrillDown,boolean psdrilldown, String tbType,String selectproject)
	{
		
					
		ArrayList<Object> serverParams = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams.add(new Object[]{accountName,fromDate,toDate,globals.session[2],ProjectName});
		serverParams.add(globals.session[0]);
			
			try {
					Object[] result_f = (Object[]) globals.client.execute("reports.getLedger", serverParams);
					String oldselectproject = null;
					String oldenddate = null;
					String oldprojectname = null;
					boolean narration =narrationFlag;
					String oldaccname = null;
					String oldfromdate = null ;
					boolean dualledgerflag=false;
					ArrayList<transaction> lstLedger = new ArrayList<transaction>();
			for(int ledgercounter =0; ledgercounter < result_f.length; ledgercounter++)
			{
				String voucherdate;
				String  particulars = null;
				String voucherno;
				String dr;
				String cr;
				String narration1;
				String voucherCode =  "";
				Object[] ledgerRow = (Object[]) result_f[ledgercounter];
				Object[] p_list =(Object[])ledgerRow[1] ;
				voucherdate = ledgerRow[0].toString();
				for(int p =0; p<p_list.length; p++)
				{
					particulars= p_list[p].toString()+"\n";
				}
				particulars = particulars.substring(0, particulars.length()-1 );
				voucherno = ledgerRow[2].toString();
				dr = ledgerRow[3].toString();
				cr =  ledgerRow[4].toString();
				narration1  = ledgerRow[5].toString();
				voucherCode= ledgerRow[6].toString();
				transaction t = new transaction(voucherdate, particulars, voucherno, dr, cr, narration1,voucherCode );
				lstLedger.add(t);
				
			}
					ViewLedgerReport ledger = new ViewLedgerReport(grandParent, SWT.None,lstLedger ,ProjectName,oldprojectname, narrationFlag,narration, accountName,oldaccname,fromDate,oldfromdate,toDate,oldenddate,tbDrillDown,psdrilldown,tbType,selectproject,oldselectproject,dualledgerflag);
					ledger.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
					System.out.print("Project name is :"+ProjectName);
			//now make an instance of the ledgerReport which is a composite.
			//in the constructor pass this result as a parameter.
			// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
	}
		
		
		}
	
	
	
	public static void showDualLedger(Composite grandParent, String accountName,String oldaccName,String fromDate,String oldfromdate,String toDate,String oldenddate,String ProjectName,String oldprojectName, boolean narrationFlag,boolean narration, boolean tbDrillDown,boolean tbflag,boolean psdrilldown,boolean projectflag, String tbType,String tb,String selectproject,String oldselectproject,boolean dualledgerflag,boolean dualflag)
	{
		
		
		ArrayList<Object> serverParams = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams.add(new Object[]{accountName,fromDate,toDate,globals.session[2],ProjectName});
		serverParams.add(globals.session[0]);
		ArrayList<Object> serverParams1 = new ArrayList<Object>();
		//code for sending project name back to ledger report
		serverParams1.add(new Object[]{oldaccName,oldfromdate,oldenddate,globals.session[2],oldprojectName});
		serverParams1.add(globals.session[0]);
		
		try {
				Object[] result_t1 = (Object[]) globals.client.execute("reports.getLedger", serverParams);
				Object[] result_t2 = (Object[]) globals.client.execute("reports.getLedger", serverParams1);
				
				ViewDualLedgr ledger_t1 = new ViewDualLedgr(grandParent, SWT.None, result_t1,result_t2,ProjectName,oldprojectName, narrationFlag,narration, accountName,oldaccName,fromDate,oldfromdate,toDate,oldenddate,tbDrillDown,tbflag,psdrilldown,projectflag,tbType,tb,selectproject,oldselectproject,dualledgerflag,dualflag);
						
				System.out.print("Project name is :"+ProjectName);
		//now make an instance of the ledgerReport which is a composite.
		//in the constructor pass this result as a parameter.
		// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
	} catch (XmlRpcException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} 
		
		
		
	}

			
		public static void showCashFlow(Composite grandParent, String fromDate, String toDate, String financialFrom)
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(new Object[]{fromDate,toDate,financialFrom});
			serverParams.add(globals.session[0]);
			try {
				Object[] result = (Object[]) globals.client.execute("reports.getCashFlow" , serverParams);
				ViewCashFlowReport CashFlow = new ViewCashFlowReport(grandParent, SWT.NONE, result, fromDate,toDate, financialFrom);
				
			} catch (XmlRpcException e)
			{
				e.printStackTrace();
			}
		}
		
		public static void getAccountReport(Composite grandParent)
		{
			try
			{
				Object[] result=(Object[])globals.client.execute("account.getAccountReport",new Object[]{globals.session[0]});
				ArrayList<accountReport> accdata = new ArrayList<accountReport>();
				for (int tbcounter = 0; tbcounter < result.length; tbcounter++)
				{
					Object[] tbRow = (Object[]) result[tbcounter];
					String srNo = tbRow[0].toString();
					System.out.println(srNo);
					String accountName = tbRow[1].toString();
					System.out.println(accountName);
					String groupName = tbRow[2].toString();
					System.out.println(groupName);
					String subgroupName = tbRow[3].toString();
					if(subgroupName.equals("None"))
					{
						subgroupName="";
					}
					System.out.println(subgroupName);
					accountReport accreport =new accountReport(srNo, accountName, groupName, subgroupName);
					accdata.add(accreport);
										
				}
				AccountReport ar=new AccountReport(grandParent, SWT.NONE,accdata);
				ar.setSize(grandParent.getClientArea().width, grandParent.getClientArea().height);
			}
			catch (XmlRpcException e)
			{
				e.printStackTrace();
			}
			
		}
	
		public static String[] getBankList()
		{
			try
			{
				Object[] result = (Object[]) globals.client.execute("reports.getBankList",new Object[]{globals.session[0]});
				String[] accounts = new String[result.length];
				for(int i = 0; i<result.length; i++)
				{

					accounts[i] = result[i].toString();
				}
				return accounts;
			}
			catch(Exception e)
			{
				e.getMessage();
				return new String[]{};
			}
		}

		public static void setReconcile(Composite grandParent, Object[][] reconData, String selectaccountname, String FromDate, String Todate, String fromYear, String projectname,Boolean narration )
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			serverParams.add(reconData);
			serverParams.add(globals.session[0]);
			
			System.out.println(selectaccountname + FromDate + Todate + projectname);
			System.out.println( globals.session[2]);
			try {
				Object result = globals.client.execute("reports.setBankRecon", serverParams );
				System.out.println( result.toString());
				Boolean successflag = Boolean.valueOf(result.toString());
				if(successflag)
				{
					serverParams.clear();
					serverParams.add(new Object[]{selectaccountname,FromDate,Todate,globals.session[2],projectname});
					serverParams.add(globals.session[0]);
					Object[] result_f = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
				
					System.out.println("selectbank:"+selectaccountname+"fromdate"+FromDate+"todate"+Todate+"fin:"+globals.session[2]+"proj:"+projectname);
					updateBankRecon ubr=new updateBankRecon(grandParent, SWT.NONE, selectaccountname,FromDate, Todate,fromYear,projectname,narration, result_f);
				}
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch bloc
				System.out.println("some thing is wrong, chamari");
				e.getMessage();
			}
			
		}
		
		
		public static void showledgerRecon(Composite grandParent, String bankname,String fromDate,String toDate, String projname,boolean narrationFlag)
		{
			
						
			ArrayList<Object> serverParams = new ArrayList<Object>();
			//code for sending project name back to ledger report
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],"No Project"});
			
			serverParams.add(globals.session[0]);
			//boolean narration =narrationFlag;
			//String ProjName = projname;
				
				try {
						Object[] result_f = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
						LedgerRecon lr = new LedgerRecon(grandParent,SWT.None,bankname,fromDate,toDate,  narrationFlag,projname,result_f);
					
				//now make an instance of the ledgerReport which is a composite.
				//in the constructor pass this result as a parameter.
				// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		}
			
			
		}
		
		public static void getClearedUnclearedTransactions (Composite grandParent, String bankname,String fromDate,String toDate,String projname,boolean narration_Flag) 
		{
			Vector<Object> reconResult = new Vector<Object>();
			ArrayList<Object> serverParams = new ArrayList<Object>();
			//code for sending project name back to ledger report
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],"No Project"});
			serverParams.add(globals.session[0]);
			System.out.println("getuncleared");
			//System.out.println(bankname + fromDate + toDate + projname+cleared_Flag);	
				try 
				{
					
					/*if(cleared_Flag==true)
					{
						*/Object[] result_cleared=(Object[]) globals.client.execute("reports.getReconciledTransactions",serverParams);
						reconResult.add(result_cleared);
						Object[] result_Uncleared = (Object[]) globals.client.execute("reports.updateBankRecon",serverParams);
						reconResult.add(result_Uncleared);
						//System.out.println("selectbank:"+bankname+"fromdate"+fromDate+"todate"+toDate+"proj:"+projname+"narration"+narration_Flag+"clear:"+cleared_Flag);

						ViewUnclearedAccounts vuca = new ViewUnclearedAccounts(grandParent, SWT.NONE, bankname, fromDate, toDate, narration_Flag,projname,reconResult  );
						
					/*}
					else
					{
						Object[] result_Uncleared = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
						reconResult.add(result_Uncleared);
						//LedgerRecon lr = new LedgerRecon(grandParent,SWT.None,bankname,fromDate,toDate, narration_Flag,projname, result);
						ViewUnclearedAccounts vuca = new ViewUnclearedAccounts(grandParent, SWT.NONE, bankname, fromDate, toDate, narration_Flag,projname,cleared_Flag ,reconResult  );

					
					}*/
				//now make an instance of the ledgerReport which is a composite.
				//in the constructor pass this result as a parameter.
				// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		public static boolean deleteClearedRecon(String accountName,int vouchercode,String clearedDate)
		{
			ArrayList<Object> serverParams = new ArrayList<Object>();
			try {
				serverParams.add(new Object[]{accountName,vouchercode,clearedDate});
				serverParams.add(globals.session[0]);
				
				Object success = globals.client.execute("reports.deleteClearedRecon", serverParams);
				return true;
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		
		public static void showUpdateRecon(Composite grandParent, String bankname,String fromDate,String toDate, String projname,boolean narrationFlag)
		{
						
			ArrayList<Object> serverParams = new ArrayList<Object>();
			//code for sending project name back to ledger report
			serverParams.add(new Object[]{bankname,fromDate,toDate,globals.session[2],projname});
			serverParams.add(globals.session[0]);
			boolean narration =false;
				
				try {
						Object[] result_f = (Object[]) globals.client.execute("reports.updateBankRecon", serverParams);
						
						//LedgerRecon lr = new LedgerRecon(grandParent,bankname,fromDate,toDate, SWT.None, result_f,narrationFlag);
				
						//System.out.print("Project name is :"+ProjectName);
				//now make an instance of the ledgerReport which is a composite.
				//in the constructor pass this result as a parameter.
				// in the function of constructor of that composite, create a table and run the loop for this grid (result ) which you took as a parameter.
			} catch (XmlRpcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			
		}
			
			
		}
				
		

}
